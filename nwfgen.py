# nwfgen.py

__appname__ = "NWF Generator"
__appver__ = "1.0.0"
__appdesc__ = "Program to generate Neutral Wirelist Format (.nwf) files"
__appurl__ = "www.gitlab.com/milanmartikan/nwf-generator"
__author__ = "Milan Martikán"

import platform
if platform.system() == "Windows":
    from ctypes import windll
    windll.shcore.SetProcessDpiAwareness(1)

from tkinter import *
from tkinter import ttk

APPWIN_BACK_COLOR = "gray20"
GUI_BACKGROUND = "gray80"
GUI_LINE_COLOR = "gray60"
GUI_LINE_WIDTH = 2
BUTTON_OFFSET_X = 10
HEADING_OFFSET_X = 10
HEADING_OFFSET_Y = 6 
LABEL_OFFSET_X = 6
LABEL_OFFSET_Y = 2
MAX_CONNECTORS_COUNT = 5
MAX_SPOOLS_COUNT = 4
WIRE_SUFFIX = "abcd"
MAX_CONNECTIONS_COUNT = len(WIRE_SUFFIX)
WIRE_CORE_COLOR = "DarkOrange1" #These three has no effect on
WIRE_INSUL_COLOR = "DeepSkyBlue4" #windows operating system,
CABLE_INSUL_COLOR = "LightSalmon4" #but functional on linux.
HELP_WIN_WIDTH = 800 #pixels

def clean_selection(event):
    event.widget.selection_clear()

def set_ttkentry(entry, text):
    entry.delete(0, "end")
    entry.insert(0, text)

class Log:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):
        self._log = list()

    def messages_area(self, ttklabel):
        self._shown_message = ttklabel

    def append(self, message):
        self._log.append(str(len(self._log)).rjust(3, "0") + " " + message)
        self._shown_message["text"] = self._log[-1]

log = Log()

class Spool:
    density = { #kg/m^3
        "metals": {
            "Cu": 8960, "Al": 2700, "Ag": 10500, "W": 19250, "Ni": 8908},
        "insulations": {
            "PVC": 1425, "PP": 925, "PE": 935, "PUR": 1150, "CPE": 975,
            "Nylon": 1070, "TPR": 1050, "SBR": 1100, "Silicone": 1600,
            "EPR": 1025, "XLPE": 945, "CSPE": 1250, "EPDM": 1200, "PTFE": 2200}
    }

    def __init__(self, app):
        self.labels = []
        self.units = []
        self.label = None
        self._name = ttk.Entry(app.spool_data_pane)
        self._spool_type = "wire_spool"
        self._thickness = ttk.Entry(app.spool_data_pane)
        self._min_bend_radius = ttk.Entry(app.spool_data_pane)
        self._lin_density = ttk.Entry(app.spool_data_pane)
        self._num_of_conductors = ttk.Entry(app.spool_data_pane)
        self.calc_btt = ttk.Button(app.spool_data_pane, text="Density Calculator",
                                   command=self.calculate_density)

    @property
    def name(self):
        return self._name.get()
    @name.setter
    def name(self, name):
        set_ttkentry(self._name, name)
    @property
    def spool_type(self):
        return self._spool_type
    @spool_type.setter
    def spool_type(self, txt):
        self._spool_type = txt
    @property
    def thickness(self):
        return self._thickness.get()
    @thickness.setter
    def thickness(self, fi):
        set_ttkentry(self._thickness, fi)
    @property
    def min_bend_radius(self):
        return self._min_bend_radius.get()
    @min_bend_radius.setter
    def min_bend_radius(self, r):
        set_ttkentry(self._min_bend_radius, r)
    @property
    def num_of_conductors(self):
        return self._num_of_conductors.get()
    @property
    def lin_density(self):
        return self._lin_density.get()
    @lin_density.setter
    def lin_density(self, density):
        set_ttkentry(self._lin_density, density)

    def calculate_density(self):
        try:
            if self.spool_type[0] == "c" and int(self.num_of_conductors) < 1:
                log.append("Number of Conductors has to be >= 1!")
                return
        except(ValueError):
            log.append("No conductors!")
            return

        self.calc_win = Toplevel()
        self.calc_win.geometry("+900+500")
        self.calc_win.resizable(FALSE, FALSE)
        self.calc_win.columnconfigure(0, weight=0)
        self.calc_win.rowconfigure(0, weight=0)
        self.calc_win["background"] = APPWIN_BACK_COLOR
        self.calc_win.title(f"Density calculator for {self.name}")
        self.calc_win.protocol("WM_DELETE_WINDOW", self.cancel_density)

        self.calc_labels = []

        self.calc_btt.state(["disabled"])

        self.calc_frame = ttk.Frame(self.calc_win, style="Back.TFrame")
        self.calc_frame.columnconfigure(0, weight=0)
        self.calc_frame.columnconfigure(1, weight=1)
        self.calc_frame.rowconfigure(0, weight=0)
        self.calc_frame.rowconfigure(1, weight=0)
        self.calc_frame.grid(column=0, row=0, sticky="nesw")

        self.scheme_pane = ttk.Frame(self.calc_frame, style="Front.TFrame", padding=10)
        self.scheme_pane.grid(column=0, row=0, sticky="nesw")

        self.cable_type = ttk.Label(self.scheme_pane, text="Wire Scheme" if
                                    self.spool_type[0] == "w" else "Cable Scheme",
                                    style="Normal.TLabel")
        self.cable_type.grid(column=0, row=0)


        self.entry_pane = ttk.Frame(self.calc_frame, style="Front.TFrame",
                                           padding=(0, 16, 10, 0))
        self.entry_pane.columnconfigure(0, weight=0)
        self.entry_pane.columnconfigure(1, weight=1)
        self.entry_pane.columnconfigure(2, weight=0)
        self.entry_pane.grid(column=1, row=0, sticky="nesw") 

        self.button_pane = ttk.Frame(self.calc_frame, style="Front.TFrame", padding=10)
        self.button_pane.columnconfigure(0, weight=1)
        self.button_pane.columnconfigure(1, weight=1)
        self.button_pane.rowconfigure(0, weight=0)
        self.button_pane.grid(column=0, columnspan=2, row=1, sticky="nesw")

        self.use_btt = ttk.Button(self.button_pane, text="Calculate Density",
                                  command=self.use_density)
        self.use_btt.grid(column=0, row=0, padx=(BUTTON_OFFSET_X,
                          BUTTON_OFFSET_X/2), pady=(6, 4), sticky="we")

        self.cancel_btt = ttk.Button(self.button_pane, text="Cancel Calculation",
                                  command=self.cancel_density)
        self.cancel_btt.grid(column=1, row=0, padx=(BUTTON_OFFSET_X,
                             BUTTON_OFFSET_X/2), pady=(6, 4), sticky="we")

        #Scheme
        cw, ch = 100, 100
        canvas = Canvas(self.scheme_pane, width=cw, height=ch, background=GUI_BACKGROUND,
                        borderwidth=0, highlightthickness=0)
        canvas.grid(column=0, row=1, pady=5)

        di = 86
        cx, cy = (cw - di)/2, (ch - di)/2
        canvas.create_oval(cx, cy, cx + di, cy + di, fill=WIRE_INSUL_COLOR if
                           self.spool_type[0] == "w" else CABLE_INSUL_COLOR, width=0)

        if self.spool_type[0] == "w":
            dc = 50
            cx, cy = (cw - dc)/2, (ch - dc)/2
            canvas.create_oval(cx, cy, cx + dc, cy + dc, fill=WIRE_CORE_COLOR, width=0)
        else:
            di = 36
            dc = 20
            dd = (di - dc) / 2

            cx, cy = (cw/2 - di/2), (ch/2 - di - 2)
            canvas.create_oval(cx, cy, cx + di, cy + di, fill=WIRE_INSUL_COLOR, width=0)
            cx, cy = (cw/2 - dc/2), (ch/2 - dc - dd - 2)
            canvas.create_oval(cx, cy, cx + dc, cy + dc, fill=WIRE_CORE_COLOR, width=0)

            cx, cy = (cw/2 - di + 1), (ch/2 - dd + 1)
            canvas.create_oval(cx, cy, cx + di, cy + di, fill=WIRE_INSUL_COLOR, width=0)
            cx, cy = (cw/2 - dc - dd + 1), (ch/2 + 1)
            canvas.create_oval(cx, cy, cx + dc, cy + dc, fill=WIRE_CORE_COLOR, width=0)

            cx, cy = (cw/2 - 1), (ch/2 - dd + 1)
            canvas.create_oval(cx, cy, cx + di, cy + di, fill=WIRE_INSUL_COLOR, width=0)
            cx, cy = (cw/2 + dd - 1), (ch/2 + 1)
            canvas.create_oval(cx, cy, cx + dc, cy + dc, fill=WIRE_CORE_COLOR, width=0)

        self.core_dia = ttk.Entry(self.entry_pane, style="Wire.TEntry")
        self.core_insul_dia = ttk.Entry(self.entry_pane, style="Insul.TEntry")
        self.core_mat = ttk.Combobox(self.entry_pane, state="readonly",
                                          values=tuple(self.density["metals"].keys()))
        self.core_mat.current(0)
        self.core_mat.bind("<<ComboboxSelected>>", clean_selection)

        self.core_insul_mat = ttk.Combobox(self.entry_pane, state="readonly",
                                           values=tuple(self.density["insulations"].keys()))
        self.core_insul_mat.current(0)
        self.core_insul_mat.bind("<<ComboboxSelected>>", clean_selection)

        if self.spool_type[0] == "c":
            self.cable_dia = ttk.Entry(self.entry_pane, style="Cable.TEntry")
            if self.thickness.count("#") != 1:
                self.thickness = f"{self.thickness}#"
            self.cable_dia.insert(0, self.thickness.split("#")[0])
            self.cable_insul_mat = ttk.Combobox(self.entry_pane, state="readonly",
                                               values=tuple(self.density["insulations"].keys()))
            self.cable_insul_mat.current(0)
            self.cable_insul_mat.bind("<<ComboboxSelected>>", clean_selection)
        
        self.core_insul_dia.insert(0,
                    self.thickness.split("#")[0 if self.spool_type[0] == "w" else 1])

        for i, n in enumerate(["Conductive Core Diameter", "Core Insulation Diameter",
                               "Overall Cable Diameter", "Conductive Core Material",
                               "Core Insulation Material", "Cable Insulation Material"]):

            self.calc_labels.append(ttk.Label(self.entry_pane, style="Normal.TLabel",
                                              text=n))

            if i in (2, 5) and self.spool_type[0] != "c":
                continue
            self.calc_labels[i].grid(column=0, row=i, padx=(LABEL_OFFSET_X, 0),
                                     pady=LABEL_OFFSET_Y, sticky="e")

            if i > 2 or (self.spool_type[0] != "c" and i == 2):
                continue
            ttk.Label(self.entry_pane, text="mm", style="Normal.TLabel") \
                .grid(column=2, row=i, sticky="w", padx=(0, 5))

        for i, n in enumerate(["core_dia", "core_insul_dia", "cable_dia",
                               "core_mat", "core_insul_mat", "cable_insul_mat"]):

            if i in (2, 5) and self.spool_type[0] != "c":
                continue
            vars(self)[n].grid(column=1, columnspan=(1 if i in (0, 1, 2) else 2),
                               row=i, padx=LABEL_OFFSET_X,
                               pady=(0, LABEL_OFFSET_Y), sticky="we")

    def use_density(self):
        try:
            ratio = float(self.core_dia.get())**2 / float(self.core_insul_dia.get())**2
        except(ValueError, ZeroDivisionError):
            return

        if ratio > 1:
            log.append("Core diameter bigger than insulation diameter!")
            return

        wire_area = (3.14 * float(self.core_insul_dia.get()) ** 2) / 4
        core_mass = ratio * wire_area * int(self.density["metals"][self.core_mat.get()])
        core_insul_mass = (1 - ratio) * wire_area * \
                          int(self.density["insulations"][self.core_insul_mat.get()])
        unit_mass = core_mass + core_insul_mass

        if self.spool_type[0] == "c":
            try:
                cable_area = (3.14 * float(self.cable_dia.get()) ** 2) / 4
                ratio = (int(self.num_of_conductors) * wire_area) / cable_area
            except(ValueError):
                return

            if float(self.core_insul_dia.get()) > float(self.cable_dia.get()):
                log.append("Core insulation bigger than cable diameter!")
                return

            cable_insul_mass = (1 - ratio) * cable_area * \
                               int(self.density["insulations"][self.cable_insul_mat.get()])
            unit_mass *= int(self.num_of_conductors)
            unit_mass += cable_insul_mass

        self.lin_density = round(unit_mass / 1000, 1) #g/m
        self.thickness = f"{self.cable_dia.get()+'#' if self.spool_type[0] == 'c' else ''}{self.core_insul_dia.get()}"
        self.cancel_density()

    def cancel_density(self):
        self.calc_win.destroy()
        self.calc_btt.state(["!disabled"])

class Connector:
    def __init__(self, app):
        self.labels = []
        self.label = None
        self._name = ttk.Entry(app.conn_data_pane)
        self._filename = ttk.Entry(app.conn_data_pane)
        self._num_pins = ttk.Entry(app.conn_data_pane)
        self._ucs_names = ttk.Entry(app.conn_data_pane)

    @property
    def name(self):
        return self._name.get()
    @name.setter
    def name(self, name):
        set_ttkentry(self._name, name)
    @property
    def filename(self):
        return self._filename.get()
    @filename.setter
    def filename(self, filename):
        set_ttkentry(self._filename, filename)
    @property
    def num_pins(self):
        return self._num_pins.get()
    @num_pins.setter
    def num_pins(self, num_pins):
        set_ttkentry(self._num_pins, num_pins)
    @property
    def ucs_names(self):
        return self._ucs_names.get()
    @ucs_names.setter
    def ucs_names(self, ucs_names):
        set_ttkentry(self._ucs_names, ucs_names)

class Connection:
    def __init__(self, app):
        self.labels = []
        self._wire_name = ttk.Entry(app.wire_data_pane)
        self.spool = ttk.Combobox(app.wire_data_pane)
        self.conn_from = ttk.Combobox(app.wire_data_pane)
        self.conn_to = ttk.Combobox(app.wire_data_pane)
        self._path = ttk.Entry(app.wire_data_pane)

    @property
    def wire_name(self):
        return self._wire_name.get()
    @wire_name.setter
    def wire_name(self, wire_name):
        set_ttkentry(self._wire_name, wire_name)
    @property
    def path(self):
        return self._path
    @path.setter
    def path(self, path):
        set_ttkentry(self._path, path)

class Application:
    win = Tk()
    win.title(__appname__)
    win.minsize(850, 400)
    win.geometry("+550+250")
    win["background"] = APPWIN_BACK_COLOR
    win.columnconfigure(0, weight=1)
    win.rowconfigure(0, weight=1)

    style = ttk.Style()
    style.configure("Back.TFrame", background=GUI_LINE_COLOR, borderwidth=0)
    style.configure("Front.TFrame", background=GUI_BACKGROUND, borderwidth=0)
    style.configure("Heading.TLabel", background=GUI_BACKGROUND, font=("Calibri Bold", 10))
    style.configure("Normal.TLabel", background=GUI_BACKGROUND, font=("Calibri", 10))
    style.configure("TButton", anchor="center", padding=(10,4,10,6), font=("Calibri", 10)) 
    style.configure("TEntry", borderwidth=0, padding=(4, 1, 4, 6), fieldbackground="snow2")
    style.configure("Wire.TEntry", fieldbackground=WIRE_CORE_COLOR)
    style.configure("Insul.TEntry", fieldbackground=WIRE_INSUL_COLOR)
    style.configure("Cable.TEntry", fieldbackground=CABLE_INSUL_COLOR)
    style.configure("TCombobox", borderwidth=0, padding=(4, 5, 4, 2), background="snow2")
    ttk.Style().map("TCombobox", fieldbackground=[("readonly", "snow2")])

    back_frame = ttk.Frame(win, style="Back.TFrame")
    back_frame.columnconfigure(0, weight=1)
    back_frame.columnconfigure(1, weight=1)
    back_frame.columnconfigure(2, weight=1)
    back_frame.rowconfigure(0, weight=1)
    back_frame.rowconfigure(1, weight=0)
    back_frame.grid(column=0, row=0, sticky="nesw")

    spool_pane = ttk.Frame(back_frame, style="Front.TFrame", padding=10)
    spool_pane.columnconfigure(0, weight=1)
    spool_pane.rowconfigure(0, weight=0)
    spool_pane.rowconfigure(1, weight=1)
    spool_pane.grid(column=0, row=0, padx=(0, GUI_LINE_WIDTH), sticky="nesw") 

    conn_pane = ttk.Frame(back_frame, style="Front.TFrame", padding=10)
    conn_pane.columnconfigure(0, weight=1)
    conn_pane.rowconfigure(0, weight=0)
    conn_pane.rowconfigure(1, weight=1)
    conn_pane.grid(column=1, row=0, padx=(0, GUI_LINE_WIDTH), sticky="nesw") 

    wire_pane = ttk.Frame(back_frame, style="Front.TFrame", padding=10)
    wire_pane.columnconfigure(0, weight=1)
    wire_pane.rowconfigure(0, weight=0)
    wire_pane.rowconfigure(1, weight=1)
    wire_pane.grid(column=2, row=0, sticky="nesw") 

    info_pane = ttk.Frame(back_frame, style="Front.TFrame")
    info_pane.rowconfigure(0, weight=1)
    info_pane.grid(column=0, row=1, padx=(0, GUI_LINE_WIDTH),
                   pady=(GUI_LINE_WIDTH, 0), sticky="nesw")
    shown_message = ttk.Label(info_pane, style="Normal.TLabel")
    shown_message.grid(row=0, padx=4, pady=0, sticky="w")

    file_pane = ttk.Frame(back_frame, style="Front.TFrame")
    file_pane.columnconfigure(0, weight=0)
    file_pane.columnconfigure(1, weight=1)
    file_pane.columnconfigure(2, weight=0)
    file_pane.columnconfigure(3, weight=0)
    file_pane.rowconfigure(0, weight=1)
    file_pane.grid(column=1, columnspan=2, row=1, pady=(GUI_LINE_WIDTH, 0), sticky="nesw")

    spool_tool_pane = ttk.Frame(spool_pane, style="Front.TFrame")
    spool_tool_pane.columnconfigure(0, weight=1)
    spool_tool_pane.columnconfigure(1, weight=1)
    spool_tool_pane.rowconfigure(0, weight=1)
    spool_tool_pane.rowconfigure(1, weight=1)
    spool_tool_pane.grid(column=0, row=0, sticky="nesw")

    conn_tool_pane = ttk.Frame(conn_pane, style="Front.TFrame")
    conn_tool_pane.columnconfigure(0, weight=1)
    conn_tool_pane.columnconfigure(1, weight=1)
    conn_tool_pane.rowconfigure(0, weight=1)
    conn_tool_pane.rowconfigure(1, weight=1)
    conn_tool_pane.grid(column=0, row=0, sticky="nesw")

    wire_tool_pane = ttk.Frame(wire_pane, style="Front.TFrame")
    wire_tool_pane.columnconfigure(0, weight=1)
    wire_tool_pane.columnconfigure(1, weight=1)
    wire_tool_pane.rowconfigure(0, weight=1)
    wire_tool_pane.rowconfigure(1, weight=1)
    wire_tool_pane.grid(column=0, row=0, sticky="nesw")

    spool_data_pane = ttk.Frame(spool_pane, style="Front.TFrame")
    spool_data_pane.columnconfigure(0, weight=0)
    spool_data_pane.columnconfigure(1, weight=1)
    spool_data_pane.columnconfigure(2, weight=0)
    spool_data_pane.rowconfigure(0, weight=1)
    spool_data_pane.grid(column=0, row=1, sticky="new")

    conn_data_pane = ttk.Frame(conn_pane, style="Front.TFrame")
    conn_data_pane.columnconfigure(0, weight=0)
    conn_data_pane.columnconfigure(1, weight=1)
    conn_data_pane.rowconfigure(0, weight=1)
    conn_data_pane.grid(column=0, row=1, sticky="new")

    wire_data_pane = ttk.Frame(wire_pane, style="Front.TFrame")
    wire_data_pane.columnconfigure(0, weight=0)
    wire_data_pane.columnconfigure(1, weight=1)
    wire_data_pane.rowconfigure(0, weight=1)
    wire_data_pane.grid(column=0, row=1, sticky="new")

    #Spools pane
    spool_type = ttk.Combobox(spool_tool_pane, state="readonly",
                              values=("Wire spool", "Cable Spool"), width=12)
    spool_type.current(0)
    spool_type.grid(column=0, row=1, padx=(BUTTON_OFFSET_X, 0), sticky="w")
    ttk.Label(spool_tool_pane, text="Spools", style="Heading.TLabel") \
        .grid(column=1, row=1, padx=(HEADING_OFFSET_X, 2),
              pady=HEADING_OFFSET_Y, sticky="w")
    spools_num = ttk.Label(spool_tool_pane, style="Heading.TLabel", text="(0)")
    spools_num.grid(column=1, row=1, padx=(2, HEADING_OFFSET_X),
                    pady=HEADING_OFFSET_Y, sticky="e")

    #Connectors pane
    ttk.Label(conn_tool_pane, text="Connectors", style="Heading.TLabel") \
        .grid(column=1, row=1, padx=(HEADING_OFFSET_X), pady=HEADING_OFFSET_Y, sticky="w")
    connectors_num = ttk.Label(conn_tool_pane, style="Heading.TLabel")
    connectors_num.grid(column=1, row=1, padx=(2, HEADING_OFFSET_X),
                        pady=HEADING_OFFSET_Y, sticky="e")

    #Connections pane
    ttk.Label(wire_tool_pane, text="Connections", style="Heading.TLabel") \
        .grid(column=1, row=1, padx=(HEADING_OFFSET_X), pady=HEADING_OFFSET_Y, sticky="w")
    connections_num = ttk.Label(wire_tool_pane, text="(0)", style="Heading.TLabel")
    connections_num.grid(column=1, row=1, padx=(2, HEADING_OFFSET_X),
                         pady=HEADING_OFFSET_Y, sticky="e")

    def __init__(self):
        log.messages_area(self.shown_message)
        log.append(f"This is {__appname__} v{__appver__}")

        #Spool buttons
        self.add_spool_btt = ttk.Button(self.spool_tool_pane, text="Add Spool",
                                       command=self.add_spool)
        self.add_spool_btt.grid(column=0, row=0, padx=(BUTTON_OFFSET_X, BUTTON_OFFSET_X/2),
                               pady=(6, 4), sticky="we")
        self.del_spool_btt = ttk.Button(self.spool_tool_pane, text="Delete Spool",
                                       command=self.del_spool)
        self.del_spool_btt.grid(column=1, row=0, padx=(BUTTON_OFFSET_X, BUTTON_OFFSET_X/2),
                               pady=(6, 4), sticky="we")

        self.spool_type.bind("<<ComboboxSelected>>", clean_selection)

        #Connector buttons
        self.add_conn_btt = ttk.Button(self.conn_tool_pane, text="Add Connector",
                                       command=self.add_connector)
        self.add_conn_btt.grid(column=0, row=0, padx=(BUTTON_OFFSET_X, BUTTON_OFFSET_X/2),
                               pady=(6, 4), sticky="we")
        self.del_connector_btt = ttk.Button(self.conn_tool_pane, text="Delete Connector",
                                            command=self.del_connector)
        self.del_connector_btt.grid(column=1, row=0,
                                    padx=(BUTTON_OFFSET_X, BUTTON_OFFSET_X/2),
                                    pady=(6, 4), sticky="we")

        #Connections buttons
        self.add_wire_btt = ttk.Button(self.wire_tool_pane, text="Add Connection",
                                       command=self.add_connection)
        self.add_wire_btt.grid(column=0, row=0, padx=(BUTTON_OFFSET_X, BUTTON_OFFSET_X/2),
                               pady=(6, 4), sticky="we")
        self.del_connection_btt = ttk.Button(self.wire_tool_pane, text="Delete Connection",
                                            command=self.del_connection)
        self.del_connection_btt.grid(column=1, row=0,
                                    padx=(BUTTON_OFFSET_X, BUTTON_OFFSET_X/2),
                                    pady=(6, 4), sticky="we")

        #File button
        ttk.Label(self.file_pane, style="Normal.TLabel",
                  text="NWF filename to generate:") \
            .grid(column=0, row=0, padx=(4, 2), pady=0, sticky=("w"))
        self.nwf_filename = ttk.Entry(self.file_pane)
        self.nwf_filename.grid(column=1, row=0, padx=2, sticky=("we"))
        self.generate_file_btt = ttk.Button(self.file_pane, text="Generate",
                                            command=self.generate_file)
        self.generate_file_btt.grid(column=2, row=0, padx=(2, 1), pady=1, sticky=("e"))

        #Help button
        ttk.Button(self.file_pane, text="Help", command=self.show_help).grid(column=3,
                row=0, padx=(2, 1), pady=1, sticky=("e"))

        #Initial layout
        self.add_conn_btt.invoke()
        self.add_conn_btt.invoke()
        self.add_wire_btt.invoke()

        self.win.mainloop()

    #Spools
    spools = list()

    def add_spool(self):
        count = len(self.spools)
        if not count < MAX_SPOOLS_COUNT:
            log.append("Max count for spools is " + str(MAX_SPOOLS_COUNT))
            return
        offset = 7 * count
        self.spools.append(Spool(self))
        
        spool = vars(self.spools[count])
        spool["label"] = ttk.Label(self.spool_data_pane,
                text=f"Spool {str(count+1)} (wire)" if self.spool_type.current() == 0 \
                else f"Spool {str(count+1)} (cable)", style="Heading.TLabel")
        spool["label"].grid(column=0, columnspan=3, padx=(0, 6),
                                               pady=(10, 0), sticky="e")

        if self.spool_type.current() == 1:
            self.spools[count].spool_type = "cable_spool"

        for i, n in enumerate(["Spool Name", "Number of Conductors",
                       f"Thickness of {'Cable#' if self.spool_type.current() == 1 else ''}Wire",
                               "Minimum Bend Radius", "Linear Density"]):
            if i == 1 and self.spool_type.current() == 0:
                self.spools[count].labels.append("")
                continue
            self.spools[count].labels.append(ttk.Label(self.spool_data_pane, text=n,
                                             style="Normal.TLabel"))
            self.spools[count].labels[i].grid(column=0, row=i+1+offset,
                padx=(LABEL_OFFSET_X, 0), pady=LABEL_OFFSET_Y, sticky="e")

        for i, n in enumerate(["_name", "_num_of_conductors", "_thickness",
                               "_min_bend_radius", "_lin_density"]):
            if i == 1 and self.spool_type.current() == 0: #Wire
                continue
            spool[n].grid(columnspan=2 if i in (0, 1) else 1,
                          column=1, row=i+1+offset, padx=LABEL_OFFSET_X,
                          pady=(0, LABEL_OFFSET_Y), sticky="we")
            if i in (2, 3, 4):
                self.spools[count].units.append(ttk.Label(self.spool_data_pane,
                                                text="mm" if i in (2, 3) else "g/m",
                                                style="Normal.TLabel"))
                self.spools[count].units[-1].grid(column=2, row=i+1+offset, padx=(0, 5), sticky="w")
            spool["calc_btt"].grid(column=1, columnspan=1, padx=LABEL_OFFSET_X,
                                   pady=(0, LABEL_OFFSET_Y), sticky="we")

        if self.spool_type.current() == 1: #Cable
           self.spools[count].thickness = "#" 
           self.spools[count].min_bend_radius = "#" 

        self.spools[count].name = f"spool{count+1}"
        spool["_name"].bind("<FocusOut>", self.update_spool_combo)

        self.update_spools_count()
        self.update_spool_combo(None)

    def update_spools_count(self):
        self.spools_num["text"] = "(" + str(len(self.spools)) + ")"

    def update_spool_combo(self, event):
        nametypes = dict()
        for spool in self.spools:
            nametypes[spool.name] = spool.spool_type
        for c in self.connections:
            values = list()
            for nt in nametypes.items():
                values.append(nt[1][0] + " " + nt[0])

            vars(c)["spool"]["values"] = values
            vars(c)["spool"].set("")

    def del_spool(self):
        if not len(self.spools) > 0:
            return
        try:
            for v in vars(self.spools.pop()).values():
                if type(v) is str:
                    del(v)
                elif type(v) is list:
                    for l in v:
                        if type(l) is str: del(l)
                        else: l.destroy()
                else:
                    v.destroy()
        except Exception as e:
            log.append(f"Error {e}")

        self.update_spools_count()
        self.update_spool_combo(None)

    #Connectors
    connectors = list()

    def add_connector(self):
        last = len(self.connectors)
        if not last < MAX_CONNECTORS_COUNT:
            log.append("Max count for connectors is " + str(MAX_CONNECTORS_COUNT))
            return
        offset = 5 * last

        self.connectors.append(Connector(self))

        conn = vars(self.connectors[last]) 
        conn["label"] = ttk.Label(self.conn_data_pane,
                text="Connector " + str(last+1), style="Heading.TLabel")
        conn["label"].grid(column=0, columnspan=2, padx=(0, 6),
                                               pady=(10, 0), sticky="e")

        for i, n in enumerate(["Connector Name", "Filename", "Number of Pins", "Entry Ports"]):
            self.connectors[last].labels.append(ttk.Label(self.conn_data_pane, text=n,
                                             style="Normal.TLabel"))
            self.connectors[last].labels[i].grid(column=0, row=i+1+offset,
                padx=(LABEL_OFFSET_X, 0), pady=LABEL_OFFSET_Y, sticky="e")
        
        for i, n in enumerate(["_name", "_filename", "_num_pins", "_ucs_names"]):
            conn[n].grid(column=1, row=i+1+offset, padx=LABEL_OFFSET_X,
                         pady=(0, LABEL_OFFSET_Y), sticky="we")
                      
        self.connectors[last].name = f"conn{last+1}"
        self.connectors[last].filename = f"file{last+1}"
        conn["_name"].bind("<FocusOut>", self.update_conn_combo)
        self.connectors[last].num_pins = 1
        self.connectors[last].ucs_names = "cs#1"

        self.update_connectors_count()
        self.update_conn_combo(None)

    def update_connectors_count(self):
        self.connectors_num["text"] = "(" + str(len(self.connectors)) + ")"

    def update_conn_combo(self, event):
        values = list()
        for conn in self.connectors:
            values.append(conn.name)
        for c in self.connections:
            vars(c)["conn_from"]["values"] = values
            vars(c)["conn_from"].set("")
            vars(c)["conn_to"]["values"] = values
            vars(c)["conn_to"].set("")

    def del_connector(self):
        if not len(self.connectors) > 2:
            return
        try:
            for v in vars(self.connectors.pop()).values():
                if type(v) is list:
                    for l in v:
                        l.destroy()
                else:
                    v.destroy()
        except Exception as e:
            log.append(f"Error {e}")

        self.update_connectors_count()
        self.update_conn_combo(None)

    #Connections
    connections = list()

    def add_connection(self):
        count = len(self.connections)
        if not count < MAX_CONNECTIONS_COUNT:
            log.append("Max count for connections is " + str(MAX_CONNECTIONS_COUNT))
            return
        offset = 6 * count

        self.connections.append(Connection(self))

        self.connections[count].labels.append(ttk.Label(self.wire_data_pane,
                    text="Connection " + str(count+1), style="Heading.TLabel"))
        self.connections[count].labels[0].grid(column=0, columnspan=2, padx=(0, 6),
                                               pady=(10, 0), sticky="e")
          
        for i, n in enumerate(["Wire Name", "Spool", "From Connector", "To Connector",
                               "Pins#Pins:Path"]):
            self.connections[count].labels.append(ttk.Label(self.wire_data_pane, text=n,
                                             style="Normal.TLabel"))
            self.connections[count].labels[i+1].grid(column=0, row=i+1+offset,
                padx=(LABEL_OFFSET_X, 0), pady=LABEL_OFFSET_Y, sticky="e")
        
        for i, n in enumerate(["_wire_name", "spool", "conn_from", "conn_to", "_path"]):
            item = vars(self.connections[count])[n]
            item.grid(column=1, row=i+1+offset, padx=LABEL_OFFSET_X,
                      pady=(0, LABEL_OFFSET_Y), sticky="we")
            if isinstance(item, ttk.Combobox):
                item.state(["readonly"])
                item.bind("<<ComboboxSelected>>", clean_selection)

        self.connections[count].wire_name = f"w{WIRE_SUFFIX[count]}"
        self.connections[count].path = "0-0#0:none"

        self.update_spool_combo(None)
        self.update_conn_combo(None)
        self.connections_num["text"] = "(" + str(len(self.connections)) + ")"

    def update_connections_count(self):
        self.connections_num["text"] = "(" + str(len(self.connections)) + ")"

    def del_connection(self):
        if not len(self.connections) > 1:
            return
        try:
            for v in vars(self.connections.pop()).values():
                if type(v) is list:
                    for l in v:
                        l.destroy()
                else:
                    v.destroy()
        except Exception as e:
            log.append(f"Error {e}")

        self.update_connections_count()

    def generate_file(self):
        try:
            nwf = open(f"{self.nwf_filename.get()}.nwf", "w", encoding="utf-8")

            #Spools
            for spool in self.spools:
                nwf.write(f"\nnew {spool.spool_type} {spool.name}")
                nwf.write(f" {spool.num_of_conductors if spool.spool_type[0] == 'c' else ''}") 
                nwf.write(f"\nparameter thickness {spool.thickness.split('#')[0]}")
                nwf.write(f"\nparameter min_bend_radius {spool.min_bend_radius.split('#')[0]}")
                nwf.write("\nparameter units mm")
                nwf.write("\nparameter mass_units gram")
                nwf.write("\nparameter density " +
                          str(round(float(spool.lin_density) / 1000, 4))) #g/mm

                if spool.spool_type[0] == "c":
                    wire_thickness = spool.thickness.split("#")[1]
                    wire_bend_radius = spool.min_bend_radius.split("#")[1]
                    for i in range(int(spool.num_of_conductors)):
                        nwf.write(f"\nconductor {i+1}")
                        nwf.write(f"\nparameter thickness {wire_thickness}")
                        nwf.write(f"\nparameter min_bend_radius {wire_bend_radius}")

            #Connectors
            for conn in self.connectors:
                nwf.write("\n\nnew connector " + conn.name)
                nwf.write("\nparameters model_name num_of_pins")
                nwf.write(" ".join(["\nvalues", conn.filename, conn.num_pins]))
                for i in range(1, 1 + int(conn.num_pins)):
                    nwf.write("\npin pin" + str(i))
                    try:
                        conn.ucs_names.index("#")
                    except:
                        log.append("Port name bad format!")
                        return
                    ucs = conn.ucs_names.split("#")
                    nwf.write("\nparameter entry_port " + ucs[0] + str(i + int(ucs[1])-1))

            #Connections
            for wires in self.connections:
                try:
                    pins_map, path_name = wires.path.get().split(":")
                    pins_from = tuple(pins_map.split("#")[0].split("-"))
                    pins_to = tuple(pins_map.split("#")[1].split("-"))
                    start_pin_from = 1 if pins_from[0] == '0' else int(pins_from[0])
                    start_pin_to = 1 if pins_to[0] == '0' else int(pins_to[0])
                    pins_count = 1 + int(conn.num_pins) if pins_map[0] == '0' \
                                 else 1 + start_pin_from + int(pins_from[1]) - \
                                      int(pins_from[0])
                except (ValueError, IndexError):
                    log.append("Pins#Pins:Path bad format!")
                    return

                nwf.write("\n")
                cable_connection = False
                for i, n in enumerate(range(start_pin_from, pins_count)):
                    spool_type = vars(wires)["spool"].get()[0]
                    if not cable_connection:
                        nwf.write(f"\nnew {'wire' if spool_type == 'w' else 'cable'} {wires.wire_name if spool_type == 'w' else 'cable'}{i+1} {vars(wires)['spool'].get()[2:]}")
                    if spool_type == "c" and not cable_connection:
                        nwf.write("\nattach " + vars(wires)["conn_from"].get() + ' ""' +
                                " " + vars(wires)["conn_to"].get() + ' ""')
                        cable_connection = True

                    if spool_type == "c":
                        nwf.write(f"\nconductor {i+1}")
                    nwf.write("\nattach " + vars(wires)["conn_from"].get() + " pin" +
                            str(n) + " " + vars(wires)["conn_to"].get() + " pin" +
                            str(start_pin_to + i))
                    if spool_type == "c":
                        nwf.write(f"\nparameter name {wires.wire_name}{i+1}")
                    if pins_map[0] != '0':
                        nwf.write(f"\nparameter use_path {path_name}")

            nwf.write("\n")
            nwf.close()

        except Exception:
            log.append("Global Error! Check your inputs.")
            return

        log.append("File generated.")
        
    def show_help(self):
        log.append("Showing help.")
        help_win = Toplevel()
        help_win.geometry("+600+100")
        help_win.resizable(FALSE, FALSE)
        help_win.columnconfigure(0, weight=0)
        help_win.rowconfigure(0, weight=0)
        help_win["background"] = APPWIN_BACK_COLOR
        help_win.title("NWF Generator Help")

        help_frame = ttk.Frame(help_win, style="Back.TFrame")
        help_frame.columnconfigure(0, weight=0)
        help_frame.rowconfigure(0, weight=1)
        help_frame.grid(column=0, row=0, sticky="nesw")

        ttk.Label(help_frame, text=__appname__, style="Heading.TLabel",
                  anchor="center", padding=(0, 15, 0, 5)).grid(sticky="ew")
        ttk.Label(help_frame, text=__appver__, style="Normal.TLabel",
                  anchor="center", padding=(0, 0, 0, 5)).grid(sticky="ew")
        ttk.Label(help_frame, text=__appdesc__, style="Normal.TLabel",
                  anchor="center", padding=(0, 0, 0, 5)).grid(sticky="ew")
        ttk.Label(help_frame, text=__appurl__, style="Normal.TLabel",
                  anchor="center", padding=(0, 0, 0, 5)).grid(sticky="ew")
        ttk.Label(help_frame, text=__author__, style="Normal.TLabel",
                  anchor="center", padding=(0, 0, 0, 25)).grid(sticky="ew")

        ttk.Label(help_frame, text="""This program serve as utility to speed up and facilitate design of cable harnesses in Creo CAD system by using its functionality to describe harness by imported logical file. The nwf file format is very simple and human readable so you could check it after generation.""",
                  style="Normal.TLabel", anchor="center", wraplength=HELP_WIN_WIDTH,
                  padding=(25, 0, 25, 25)).grid(sticky="ew")

        ttk.Label(help_frame, text="Spools", style="Heading.TLabel",
                  padding=(35, 0, 35, 0)).grid(sticky="ew")
        ttk.Label(help_frame, text="""There has to be at least one wire or cable spool defined. Besides manually define linear density it is also possible to define density of cable harness by using density calculator. By defining linear density the Creo assign mass to harness cad model. In order to use density calculator for the \"cable\" spool type it is neccessary to define number of wires that are inside cable.""", style="Normal.TLabel", anchor="w",
                  wraplength=HELP_WIN_WIDTH, padding=(35, 0, 35, 10)).grid(sticky="ew")

        ttk.Label(help_frame, text="Connectors", style="Heading.TLabel",
                  padding=(35, 0, 35, 0)).grid(sticky="ew")
        ttk.Label(help_frame, text="""Naming of the coordination systems defining the input ports of the connector in the creo file of the connector. The # character separates the name from the enumeration. If for example the input ports in the connector file will be defined by coordinate systems with names CS5 to CS10, CS#5 will be entered. For input port names e.g. EP1 to EP8 the EP#1 will be specified. Thus, the specified number indicates the first number in the numbering in the coordinate system names and this number is automatically incremented up to the specified number of pins for the \"from\" connector.""",
                  style="Normal.TLabel", anchor="w",
                  wraplength=HELP_WIN_WIDTH, padding=(35, 0, 35, 10)).grid(sticky="ew")

        ttk.Label(help_frame, text="Connections", style="Heading.TLabel",
                  padding=(35, 0, 35, 0)).grid(sticky="ew")
        ttk.Label(help_frame, text="""In the pattern Pins#Pins:Path first \"Pins\" defines start pin of \"from\" connector and second \"Pins\" defines start pin of \"to\" connector. Number of used pins is defined in connector section for the one used as \"from\" connector. If you specify something else that \"none\" for path, then this name for network path will be added to generated file. So for example if connector names would be default conn1, conn2 and conn3 and \"Number of Pins\" of conn1 is defined as 8, conn2 as 10 and conn3 as 4. Then the pattern 1-4#3:hydraulics define wires between conn1 pins range (1:4) to the conn2 pins range (3:6) using network path named \"hydraulics\". You can then add and define another connection with remaining pins, for example 5-8#1:none with conn1 and conn3 as \"from\" and \"to\" connectors respectivelly.""",
                  style="Normal.TLabel", anchor="w",
                  wraplength=HELP_WIN_WIDTH, padding=(35, 0, 35, 20)).grid(sticky="ew")

        ttk.Button(help_frame, text="Close", command=help_win.destroy).grid(sticky="ew")

Application()
