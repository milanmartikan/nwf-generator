### Introduction
This program serve as utility to speed up and facilitate design of cable harnesses in Creo CAD system by using its functionality to describe harness by imported logical file. The nwf file format is very simple and human readable so you could check it after generation.

![nwf-generator-app](app-screenshot.png)

### Usage
1. Define and generate nwf file.
2. Import this file by cabling module into Creo.
3. Automatically route cables using this logical data.

### Help
Click the button **Help** in application window.